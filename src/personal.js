import Vue from 'vue';
import App from './personal.vue';

// CSP config for webpack dynamic chunk loading
// eslint-disable-next-line
__webpack_nonce__ = btoa(OC.requestToken)

// Correct the root of the app for chunk loading
// OC.linkTo matches the apps folders
// OC.generateUrl ensure the index.php (or not)
// eslint-disable-next-line
__webpack_public_path__ = OC.linkTo('settings', 'js/')

// bind to window
Vue.prototype.t = t;
Vue.prototype.OC = OC;
Vue.prototype.OCA = OCA;
Vue.prototype.oc_userconfig = oc_userconfig;
Vue.prototype.oc_current_user = oc_current_user;

const app = new Vue({
        render: h => h(App)
}).$mount('#orga_settings');

export { app };
