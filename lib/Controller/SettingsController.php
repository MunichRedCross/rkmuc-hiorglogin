<?php
/**
 *
 * @copyright Copyright (c) 2019, Björn A. Bores <bjalbor@gmail.com>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\HiorgLogin\Controller;

use OCP\AppFramework\Controller;
use OCP\AppFramework\Http;
use OCP\AppFramework\Http\JSONResponse;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Http\RedirectResponse;
use OCP\IL10N;
use OCP\IRequest;
use OCP\IURLGenerator;
use OCP\IUserSession;
use OCP\IUserManager;
use OCP\Util;
use OCA\HiorgLogin\Service\Service;
use OCA\HiorgLogin\Service\Config;

class SettingsController extends Controller
{
    /** @var IURLGenerator */
    private $urlGenerator;
    /** @var IUserSession */
    private $userSession;
    /** @var IUserManager */
    private $userManager;
    /** @var IL10N */
    private $l;
    /** @var Service */
    private $service;
    /** @var Config */
    private $config;

    public function __construct(
        $appName,
        IRequest $request,
        Config $config,
        IURLGenerator $urlGenerator,
        IUserSession $userSession,
        IUserManager $userManager,
        IL10N $l,
        Service $service
    ) {
        parent::__construct($appName, $request);
        $this->config = $config;
        $this->urlGenerator = $urlGenerator;
        $this->userSession = $userSession;
        $this->userManager = $userManager;
        $this->l = $l;
        $this->service = $service;
    }

    /**
     * Save admin config
     *
     * @param string $default_user_group
     * @param string $default_admin_group
     * @param string $oauth2_clientId
     * @param string $oauth2_clientSecret
     * @param string $allowed_orgas
     * @param string $default_folder_quota
     * @return string JSON
     *
     * @UseSession
     */
    public function saveAdmin(
        $default_user_group,
        $default_admin_group,
        $oauth2_clientId,
        $oauth2_clientSecret,
        $allowed_orgas,
        $default_folder_quota,
        $allow_login_connect
    ) {
        $this->config->setAppValue('default_user_group', $default_user_group);
        $this->config->setAppValue('default_admin_group', $default_admin_group);
        $this->config->setAppValue('oauth2_clientId', $oauth2_clientId);
        $this->config->setAppValue('oauth2_clientSecret', $oauth2_clientSecret);
        $this->config->setAppValue('allowed_orgas', $allowed_orgas);
        $this->config->setAppValue('default_folder_quota', $default_folder_quota);
        $this->config->setAppValue('allow_login_connect', $allow_login_connect);

        // return success
        return new JSONResponse(['success' => true]);
    }

    /**
     * Save personal config
     * Not yet used
     *
     * @param string $option
     * @return string JSON
     *
     * @NoAdminRequired
     * @NoPasswordConfirmationRequired
     * @UseSession
     */
    public function savePersonal(
        $option
    ) {
        // Get User id
        $uid = $this->userSession->getUser()->getUID();

        $this->config->setUserValue($uid, 'option', $option ? 1 : 0);

        // return success
        return new JSONResponse(['success' => true]);
    }

    /**
     * Delete user from group
     *
     * Uses private Helper
     *
     * @param string $orga Hiorg Orga
     * @param string $group Name of Nextcloud group to add/delete user to
     * @param string $user Username to add / delete
     *
     * @NoAdminRequired
     * @NoPasswordConfirmationRequired
     * @UseSession
     */
    public function groupUserDelete (
        $orga,
        $group,
        $user
    ) {
        $method = $this->request->getMethod();

        return $this->groupUserAddDelete($method, $orga, $group, $user);
    }

    /**
     * Add user to group
     *
     * Uses private Helper
     *
     * @param string $orga Hiorg Orga
     * @param string $group Name of Nextcloud group to add/delete user to
     * @param string $newUser Username to add / delete
     *
     * @NoAdminRequired
     * @NoPasswordConfirmationRequired
     * @UseSession
     */
    public function groupUserAdd (
        $orga,
        $group,
        $newUser
    ) {
        $method = $this->request->getMethod();

        return $this->groupUserAddDelete($method, $orga, $group, $newUser);
    }

    /**
     * Helper to delete or add user to group
     *
     * @param string $method Called method (post for add or delete for delete)
     * @param string $orga Hiorg Orga
     * @param string $group Name of Nextcloud group to add/delete user to
     * @param string $user Username to add / delete
     *
     */
    private function groupUserAddDelete (
        $method,
        $orga,
        $group,
        $user
    ) {
        // User is authorized?
        if (false === $this->service->loginIsAdmin($this->userSession->getUser()->getUID(), $orga)) {
            return new JSONResponse([], Http::STATUS_UNAUTHORIZED);
        }

        // Group is in Orga?
        if (!$this->service->groupIsInOrga($group, $orga)) {
            return new JSONResponse([], Http::STATUS_NOT_FOUND);
        }

        if (strtolower($method) === 'post') {
            // Add User to group
            $this->service->groupAddUser($group, $user);

        } elseif (strtolower($method) === 'delete') {
            // Delete User from group
            $this->service->groupRemoveUser($group, $user);
        }

        return new JSONResponse($this->service->groupGetDetails($group));
    }

    /**
     * Delete group
     *
     * @param string $orga Hiorg Orga
     * @param string group Nextcloud group name
     *
     * @NoAdminRequired
     * @NoPasswordConfirmationRequired
     * @UseSession
     */
    public function groupDelete (
        $orga,
        $group
    ) {
        // User is authorized?
        if (false === $this->service->loginIsAdmin($this->userSession->getUser()->getUID(), $orga)) {
            return new JSONResponse([1], Http::STATUS_UNAUTHORIZED);
        }

        if (!$this->service->groupIsInOrga($group,$orga)) {
            return new JSONResponse([], Http::STATUS_UNAUTHORIZED);
        }

        if (!$this->service->groupExists($group)) {
            return new JSONResponse([], Http::STATUS_NOT_FOUND);
        }

        $groupObject = $this->service->groupGetDetails($group);

        if (count($groupObject['users']) !== 0) {
            return new JSONResponse(['errortext' => 'Es sind noch Benutzer zugeordnet!'], Http::STATUS_BAD_REQUEST);
        }

        // Remove group
        $this->service->groupRemove($group);

        return new JSONResponse([]);
    }

    /**
     * Add group
     *
     * @param string $orga Hiorg Orga
     * @param string $suffix Group suffix
     *
     * @NoAdminRequired
     * @NoPasswordConfirmationRequired
     * @UseSession
     */
    public function groupAdd (
        $orga,
        $suffix
    ) {
        // User is authorized?
        if (false === $this->service->loginIsAdmin($this->userSession->getUser()->getUID(), $orga)) {
            return new JSONResponse([], Http::STATUS_UNAUTHORIZED);
        }

        // Build group name
        $groupName = $this->config->buildGroupName($orga, $suffix);

        // Group exists?
        if ($this->service->groupExists($groupName)) {
            return new JSONResponse(['errortext' => 'Diese Gruppe existiert bereits!'], Http::STATUS_CONFLICT);
        }

        // Create group
        $this->service->groupAdd($groupName);

        // Something failed
        if (!$this->service->groupExists($groupName)) {
            return new JSONResponse([], Http::STATUS_BAD_REQUEST);
        }

        return $this->service->groupGetDetails($groupName);
    }

    /**
     * Get all groups in orga
     *
     * Uses private helper
     *
     * @param string $orga Hiorg Orga
     *
     * @NoAdminRequired
     * @NoPasswordConfirmationRequired
     * @UseSession
     */
    public function groupIndex ($orga) {
            return $this->groupShow($orga);
    }

    /**
     * Fetch groups of orga
     *
     * @param string $orga Hiorg Orga
     *
     * @NoAdminRequired
     * @NoPasswordConfirmationRequired
     * @UseSession
     */
    public function groupShow (
        $orga
    ) {
        if (false === $this->service->loginIsAdmin($this->userSession->getUser()->getUID(), $orga)) {
            return new JSONResponse([], Http::STATUS_UNAUTHORIZED);
        }

        return new JSONResponse($this->service->groupListDetails($orga));
    }

    /**
     * Delete group access to folder
     *
     * Uses private helper
     *
     * @param string $orga Hiorg Orga
     * @param string $folder group folder name
     * @param string $group Nextcloud group name

     * @NoAdminRequired
     * @NoPasswordConfirmationRequired
     * @UseSession
     */
    public function folderGroupDelete (
        $orga,
        $folder,
        $group
    ) {
        $method = $this->request->getMethod();
        return $this->folderGroupAddDelete($method, $orga, $folder, $group);
    }

    /**
     * Add group access to folder
     *
     * Uses private helper
     *
     * @param string $orga Hiorg Orga
     * @param string $folder group folder name
     * @param string $newGroup Nextcloud group name
     * @param string $mode Access mode (ro / rw)
     *
     * @NoAdminRequired
     * @NoPasswordConfirmationRequired
     * @UseSession
     */
    public function folderGroupAdd (
        $orga,
        $folder,
        $mode,
        $newGroup
    ) {
        $method = $this->request->getMethod();
        return $this->folderGroupAddDelete($method, $orga, $folder, $newGroup, $mode);
    }

    /**
     * Add or delete group access to folder
     *
     * @param string $method Called method (post for add or delete for delete)
     * @param string $orga Hiorg Orga
     * @param string $folder group folder name
     * @param string $group Nextcloud group name
     * @param string $mode Access mode (ro / rw)
     *
     */
    private function folderGroupAddDelete (
        $method,
        $orga,
        $folder,
        $group,
        $mode = null
    ) {
        // User is authorized?
        if (false === $this->service->loginIsAdmin($this->userSession->getUser()->getUID(), $orga)) {
            return new JSONResponse([], Http::STATUS_UNAUTHORIZED);
        }

        // Group is custum group in Orga
        if (!$this->service->groupIsInOrga($group, $orga)) {
                return new JSONResponse([], Http::STATUS_NOT_FOUND);
        }

        // No such group
        if (!$this->service->groupExists($group)) {
            return new JSONResponse([], Http::STATUS_NOT_FOUND);
        }

        // Folder is in Orga
        if (!$this->service->groupfolderIsInOrga($folder, $orga)) {
                return new JSONResponse([], Http::STATUS_NOT_FOUND);
        }

        // Fetch Folder Object
        $folderObject = $this->service->groupfolderGetDetails($folder);

        // Check, if group ist admin group
        $adminGroupName = $this->config->buildAdminGroupName($orga);
        $isAdminGroup = ($adminGroupName == $group);


        // No such folder
        if (null === $folderObject) {
            return new JSONResponse([], Http::STATUS_NOT_FOUND);
        }

        if (strtolower($method) === 'post' && !isset($folderObject['groups'][$group])) {
            $this->service->groupfolderAddGroup($folder, $group, ($isAdminGroup) ? 'rw' : $mode);
        } elseif (strtolower($method) === 'delete' && isset($folderObject['groups'][$group])) {
            $this->service->groupfolderRemoveGroup($folder, $group);
        } else {
            return new JSONResponse([], Http::STATUS_BAD_REQUEST);
        }

        // Success
        $folderObject = $this->service->groupfolderGetDetails($folder, $orga);

        return new JSONResponse($folderObject);
    }

    /**
     * Deletes group folder
     *
     * @param string $orga Hiorg Orga
     * @param string $folder groupfolder name
     *
     * @NoAdminRequired
     * @NoPasswordConfirmationRequired
     * @UseSession
     */
    public function folderDelete ($orga, $folder) {
        // User is authorized?
        if (false === $this->service->loginIsAdmin($this->userSession->getUser()->getUID(), $orga)) {
            return new JSONResponse([], Http::STATUS_UNAUTHORIZED);
        }

        if (!$this->service->groupfolderIsInOrga($folder, $orga)) {
            return new JSONResponse([], Http::STATUS_UNAUTHORIZED);
        }

        // Fetch Folder Object
        $folderObject = $this->service->groupfolderGetDetails($folder);

        // No such folder
        if (null === $folderObject) {
            return new JSONResponse([ ], Http::STATUS_NOT_FOUND);
        }

        // Folder is still in use. Remove all groups first
        if (count($folderObject['groups']) !== 0) {
            return new JSONResponse(['errortext' => 'Es sind noch Gruppen zugeordnet!'], Http::STATUS_BAD_REQUEST);
        }

        // Remove Folder
        $this->service->groupfolderRemove($folder);

        return new JSONResponse([]);
    }

    /**
     * Adds group folder
     *
     * @param string $orga Hiorg Orga
     * @param string $suffix group folder suffix
     *
     * @NoAdminRequired
     * @NoPasswordConfirmationRequired
     * @UseSession
     */
    public function folderAdd ($orga, $suffix) {
        // User is authorized?
        if (false === $this->service->loginIsAdmin($this->userSession->getUser()->getUID(), $orga)) {
            return new JSONResponse([], Http::STATUS_UNAUTHORIZED);
        }

        // Build group name
        $folderName = $this->config->buildFolderName($orga, $suffix);
        $adminGroupName = $this->config->buildAdminGroupName($orga);

        // Folder exists?
        if ($this->service->groupfolderExists($folderName)) {
            return new JSONResponse(['errortext' => 'Dieser Gruppenordner existiert bereits!'], Http::STATUS_CONFLICT);
        }

        // Create Folder
        $id = $this->service->groupfolderAdd(
            $folderName,
            $adminGroupName,
            $this->config->getAppValue('default_folder_quota')
        );

        // Fetch new Object
        $folderObject = $this->service->groupfolderGetDetails($folderName);

        // Something failed
        if (null === $folderObject) {
            return new JSONResponse([], Http::STATUS_BAD_REQUEST);
        }

        // return success
        return new JSONResponse($folderObject);
    }

    /**
     * List group folders of orga
     *
     * @param string $orga Hiorg Orga
     *
     * @NoAdminRequired
     * @NoPasswordConfirmationRequired
     * @UseSession
     */
    public function folderShow ($orga) {
        if (false === $this->service->loginIsAdmin($this->userSession->getUser()->getUID(), $orga)) {
            return new JSONResponse([], Http::STATUS_UNAUTHORIZED);
        }

        return new JSONResponse($this->service->groupfolderListDetails($orga));
    }

    /**
     *  Disconnects Hiorg login from user
     *
     * @param $login string Hiorg Loginname to disconnect
     *
     * @NoAdminRequired
     * @NoPasswordConfirmationRequired
     * @UseSession
     */
    public function loginDisconnect($login)
    {
        $this->service->loginDisconnect($login);
        return new RedirectResponse($this->urlGenerator->linkToRoute('settings.PersonalSettings.index', ['section'=>'hiorglogin']));
    }

    /**
     * Search for users
     *
     * @param string $searchString
     *
     * @NoAdminRequired
     * @NoPasswordConfirmationRequired
     * @UseSession
     */
    public function userSearch (
        $searchString
    ) {

            $userList = $this->userManager->search($searchString);

            if (null == $userList) {
                return new JSONResponse(['errortext' => '' ], Http::STATUS_NOT_FOUND);
            }

            $userArray = [];
            foreach ($userList as $user) {
                $displayName = $user->getDisplayName();
                $userArray[] = [ 'id' => $user->getUID(), 'name' => $user->getDisplayName() ];
            }

            if (0 == count($userArray)) {
                return new JSONResponse([ ], Http::STATUS_NOT_FOUND);
            }

            return new JSONResponse($userArray);

    }

}
