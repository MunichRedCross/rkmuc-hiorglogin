<?php
/**
 *
 * @copyright Copyright (c) 2019, Björn A. Bores <bjalbor@gmail.com>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\HiorgLogin\Controller;

use OCP\AppFramework\Controller;
use OCP\AppFramework\Http\RedirectResponse;
use OCP\IURLGenerator;
use OCP\IL10N;
use OC_Util;
use OCP\IRequest;
use OCP\IUserSession;
use OCP\IUserManager;
use OC\User\LoginException;
use Hybridauth\Provider;
use Hybridauth\User\Profile;
use Hybridauth\HttpClient\Curl;
use OCA\HiorgLogin\Service\Service;
use OCA\HiorgLogin\Service\Config;
use OC\Authentication\Token\IProvider;

class LoginController extends Controller
{
    /** @var IUserManager */
    private $userManager;
    /** @var IUserSession */
    private $userSession;
    /** @var IProvider */
    private $tokenProvider;
    /** @var IL10N */
    private $l;
    /** @var Service */
    private $service;
    /** @var Config */
    private $config;


    public function __construct(
        $appName,
        IRequest $request,
        IUserManager $userManager,
        IUserSession $userSession,
	IProvider $tokenProvider,
        IL10N $l,
        Service $service,
        Config $config
    ) {
        parent::__construct($appName, $request);
        $this->userManager = $userManager;
        $this->userSession = $userSession;
	$this->tokenProvider = $tokenProvider;
        $this->l = $l;
        $this->service = $service;
        $this->config = $config;
    }

    /**
     * 
     * Authenticate user
     * 
     * 
     * @PublicPage
     * @NoCSRFRequired
     * @UseSession
     */
    public function authLogin($loginarray)
    {
        $currentUid = null;

        $provider = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId'                => $this->config->getAppValue('oauth2_clientId'),
            'clientSecret'            => $this->config->getAppValue('oauth2_clientSecret'),
            'urlAuthorize'            => $this->config->getAppValue('oauth2_urlAuthorize'),
            'urlAccessToken'          => $this->config->getAppValue('oauth2_urlAccessToken'),
            'urlResourceOwnerDetails' => $this->config->getAppValue('oauth2_urlResourceOwnerDetails'),
        ]);

        if (!isset($_GET['code'])) {

            // Save redirect
            if ($this->request->redirect_url) {
                $_SESSION['redirect_url'] = $this->request->redirect_url;
            }
            $authorizationUrl = $provider->getAuthorizationUrl( [ 'scope' => $this->config->getAppValue('oauth2_scope') ] );
            return new RedirectResponse($authorizationUrl);
        } elseif (empty($_GET['state']) || (isset($_SESSION['oauth2state']) && $_GET['state'] !== $_SESSION['oauth2state'])) {

            if (isset($_SESSION['oauth2state'])) {
                unset($_SESSION['oauth2state']);
            }
    
            throw new LoginException("Invalid state");
        }

        try {

            // Try to get an access token using the authorization code grant.
            $accessToken = $provider->getAccessToken('authorization_code', [
                'code' => $_GET['code']
            ]);

            // Using the access token, we may look up details about the
            // resource owner.
            $rawUserData = $provider->getResourceOwner($accessToken);

        } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {

            // Failed to get the access token or user details.
            throw new LoginException($e->getMessage());

        }

        // Get logged in user
        if ($this->userSession->isLoggedIn()) {
            $currentUid = $this->userSession->getUser()->getUID();
        }

        // Munich mode?
        if ($this->config->getAppValue('autoconnect')) {
            // Get array of users from Hiorg
            $data = $rawUserData->toArray();
        } else {
            // Get single user from HiOrg
            $data = [ $rawUserData->toArray() ];
        }

        // Check, if all orgas are allowed
        $allowedOrgas = $this->config->getAppValue('allowed_orgas');
        if (!$allowedOrgas) {
            throw new LoginException($this->l->t('Keine erlaubten Orga-Kürzel definiert'));
        }
        $allowedOrgas = explode(',', $allowedOrgas);
        foreach ($data as $orgaData) {
            if (!in_array($orgaData['organisation_key'], $allowedOrgas )) {
                throw new LoginException($this->l->t('Diese HiOrg-Organisation ist nicht zum Login berechtigt'));
            }
        }

        // Traverse through orgas and get already connected logins
        $ncUidMap = [];
        foreach ($data as $orgaData) {
            $uid = $this->service->loginFindHiorgId($orgaData['sub']);
            if ($uid && !in_array($uid, $ncUidMap)) {
                $ncUidMap[] = $uid;
            }
        }

        if (2 <= count($ncUidMap)) {
            // Several different UIDs
            throw new LoginException($this->l->t('Mind. ein Benutzer ist bereits angelegt'));
        } elseif (1 == count($ncUidMap)) {
            // One UID
            if ($currentUid) {
                if ($ncUidMap[0] !== $currentUid) {
                    throw new LoginException($this->l->t('Dieser Benutzer ist bereits angelegt'));
                } else {
                    // Fetch already used UID
                    $ncUser = $currentUid;
                }
            } else {
                $ncUser = $ncUidMap[0];
            }
        } else {
            if ($currentUid) {
                if ($this->config->getAppValue('allow_login_connect') === false) {
                    throw new LoginException($this->l->t('Benutzerverknüpfungen sind nicht erlaubt'));
                }
                // Fetch already used UID
                $ncUser = $currentUid;
            } else {
                // Create new UID
                $ncUser = $this->config->buildNcUid($data[0]['sub']);
            }
        }
        
        // User is admin in at least in one orga?
        $userIsAdmin = false;

        // Connect users, create groups & folders
        foreach ($data as $orgaData) {

                $orga = $orgaData['organisation_key'];

                // Backward compability
                $orgaData['orga'] = $orga;
                $orgaData['fullname'] = $orgaData['name'];
                $orgaData['user_id'] = $orgaData['sub'];
                $orgaData['leitung'] = $orgaData['is_leitung'];

                $this->service->loginConnect($ncUser, $orgaData['sub'], $orgaData['name'], $orgaData);

                // User group
                $userGroupName = $this->config->buildUserGroupName($orga);
                $this->service->groupAdd($userGroupName);
                $this->service->groupAddUser($userGroupName, $ncUser);

                // Admin group
                $adminGroupName = $this->config->buildAdminGroupName($orga);
                $this->service->groupAdd($adminGroupName);
                if ($this->service->loginIsAdmin($ncUser, $orga)) {
                    $userIsAdmin = true;
                    $this->service->groupAddUser($adminGroupName, $ncUser);
                } else {
                    $this->service->groupRemoveUser($adminGroupName, $ncUser);
                }

                // User groupfolder
                $groupFolderName = $this->config->buildUserFolderName($orga);
                $this->service->groupfolderAdd(
                    $groupFolderName,
                    $adminGroupName,
                    $this->config->getAppValue('default_folder_quota')
                );
                $this->service->groupfolderAddGroup($groupFolderName, $userGroupName, 'ro'); // READONLY
                $this->service->groupfolderAddGroup($groupFolderName, $adminGroupName, 'rw'); // READWRITE

                // Admin groupfolder
                $groupFolderName = $this->config->buildAdminFolderName($orga);
                $this->service->groupfolderAdd(
                    $groupFolderName,
                    $adminGroupName,
                    $this->config->getAppValue('default_folder_quota')
                );
                $this->service->groupfolderAddGroup($groupFolderName, $adminGroupName, 'rw'); // READWRITE

        }

        // Set default user group
        $default_user_group =  $this->config->getAppValue('default_user_group');
        if ($default_user_group) $this->service->groupAddUser($default_user_group, $ncUser);

        // Set default admin group
        $default_admin_group =  $this->config->getAppValue('default_admin_group');
        if ($userIsAdmin) {
            if ($default_admin_group) $this->service->groupAddUser($default_admin_group, $ncUser);
        } else {
            if ($default_admin_group) $this->service->groupRemoveUser($default_admin_group, $ncUser);
        }

        // Get user
        $user = $this->userManager->get($ncUser);
        if (null === $user) {
                throw new LoginException($this->l->t('Fatal error'));
        }

        // Log in user
	$user->setDisplayName((string)$data[0]['name']);
	$this->userSession->setTokenProvider($this->tokenProvider);
        $this->userSession->createSessionToken($this->request, $user->getUID(), $user->getUID());
        $token = $this->tokenProvider->getToken($this->userSession->getSession()->getId());

        $this->userSession->completeLogin($user, [
            'loginName' => $user->getUID(),
            'password' => '',
            'token' => $token,
        ], false);

        $this->userSession->createRememberMeToken($user);
        $user->setEMailAddress((string)$data[0]['email']);

        // Saved redirect url. getDefaultPageUrl uses 'redirect_url'
        if ($_SESSION['redirect_url']) {
            $_REQUEST['redirect_url'] = $_SESSION['redirect_url'];
            unset($_SESSION['redirect_url']);
        }
        return new RedirectResponse(OC_Util::getDefaultPageUrl());
    }

}
