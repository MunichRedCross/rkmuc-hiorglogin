<?php
/**
 *
 * @copyright Copyright (c) 2019, Björn A. Bores <bjalbor@gmail.com>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\HiorgLogin;

use OC\User\Backend;
use OCA\HiorgLogin\Db\HiorgLoginUsersDAO;

class UserBackend implements \OCP\IUserBackend, \OCP\UserInterface, \OCP\User\Backend\IPasswordConfirmationBackend  {
	private $logContext = ['app' => 'hiorglogin'];
	private $hiorgLoginUsersDAO;

	public function __construct(
		HiorgLoginUsersDAO $hiorgLoginUsersDAO
	) {
		$this->hiorgLoginUsersDAO = $hiorgLoginUsersDAO;
	}

	public function getBackendName() {
		return 'Hiorg Login';
	}

	public function implementsActions($actions) {
		return (bool)((
					Backend::GET_DISPLAYNAME 
			) & $actions);
	}

        public function canConfirmPassword(string $uid): bool {
                return false;
        }

	public function hasUserListings() {
		return true;
	}

	public function deleteUser($uid) {
		return $this->hiorgLoginUsersDAO->deleteUser($uid);
	}

	public function getUsers($searchString = '', $limit = null, $offset = null) {
		return $this->hiorgLoginUsersDAO->loginFindDisplayName($searchString, $limit, $offset);
	}

	public function userExists($uid) {
		return $this->hiorgLoginUsersDAO->loginExists($uid);
	}

	public function getEMailAddress($uid) {
		return $this->hiorgLoginUsersDAO->getEMailAdress($uid);
	}

	public function getDisplayName($uid) {
		return $this->hiorgLoginUsersDAO->getDisplayName($uid);
	}

	public function getDisplayNames($search = '', $limit = null, $offset = null) {
		$users =  $this->hiorgLoginUsersDAO->loginFindDisplayName($search, $limit, $offset);
		if (count($users) == 0) return null;

		$result = [];
		foreach ($users as $user){
			$result[$user] = $this->getDisplayName($user);
		}
		return $result;
	}

}
