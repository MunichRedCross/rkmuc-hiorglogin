<?php
/**
 *
 * @copyright Copyright (c) 2019, Björn A. Bores <bjalbor@gmail.com>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\HiorgLogin\Service;

// App Group Folders
use OCA\GroupFolders\Folder\Folder;
use OCA\GroupFolders\Folder\FolderManager;
use OCP\Files\IMimeTypeLoader;
use OCA\GroupFolders\ACL\Rule;
use OCA\GroupFolders\ACL\RuleManager;
use OCP\Files\IRootFolder;
use OCA\GroupFolders\Mount\MountProvider;
use OCA\GroupFolders\ACL\UserMapping\UserMapping;


// Core
use OCP\IGroupManager;
use OCP\IUserManager;

// Service
use OCA\HiorgLogin\Service\Config;
use OCA\HiorgLogin\Db\HiorgLoginUsersDAO;


class Service {
    /** @var IGroupManager */
    private $groupManager;
    /** @var IUserManager */
    private $userManager;
    /** @var Config */
    private $config;
    /** @var HiorgUsersDAO */
    private $hiorgLoginUsersDAO;
    /** @var FolderManager */
    private $folderManager;

    private $ruleManager;
    private $rootFolder;
    private $mountProvider;


    private $groupfolderCache;

    public function __construct(
        $appName,
        IGroupManager $groupManager,
        IUserManager $userManager,
        Config $config,
        HiorgLoginUsersDAO $hiorgLoginUsersDAO,
        FolderManager $folderManager,
        RuleManager $ruleManager,
        MountProvider $mountProvider,
        IRootFolder $rootFolder
    ) {
        $this->appName = $appName;
        $this->groupManager = $groupManager;
        $this->userManager = $userManager;
        $this->config = $config;
        $this->hiorgLoginUsersDAO = $hiorgLoginUsersDAO;
        $this->folderManager = $folderManager;
        $this->ruleManager = $ruleManager;
        $this->mountProvider = $mountProvider;
        $this->rootFolder = $rootFolder;


        // App groupfolders loaded?
        /*
        if (true || !\OCP\App::isEnabled('groupfolders')
            || !class_exists('OCA\GroupFolders\Command\Group')) {

            throw new \Exception ('Gruppenordner müssen aktiv sein');
        }
        */
    }


    /**
     * Connect Hiorg-Login to nextcloud Backend Login
     *
     * @param string $nc_uid Nextcloud Userid
     * @param string $hiorg_uid Hiorg Userid
     * @param string $display_name Display Name
     * @param array $data Hiorg data array
     *
     */
    public function loginConnect($nc_uid, $hiorg_uid, $display_name, $data)
    {
        if ($this->hiorgLoginUsersDAO->loginFindHiorgId($hiorg_uid)) {
            $this->hiorgLoginUsersDAO->loginUpdate($nc_uid, $hiorg_uid, $display_name, $data);
        } else {
            $this->hiorgLoginUsersDAO->loginAdd($nc_uid, $hiorg_uid, $display_name, $data);
        }
    }

    /**
     * Disconnect Hiorg-Login from nextcloud Backend Login
     *
     * @param string $hiorg_uid Hiorg Userid
     *
     */
    public function loginDisconnect($hiorg_uid) {
            $this->hiorgLoginUsersDAO->loginDisconnect($hiorg_uid);
    }

    /**
     * Get all connected logins
     *
     * @param string $nc_uid Nextcloud Userid
     * @return array Connected Hiorg UserIds
     */
    public function loginGetConnected($nc_uid)
    {
        return $this->hiorgLoginUsersDAO->loginGetConnected($nc_uid);
    }

    /**
     * Get Orgas of user
     *
     * @param string $nc_uid
     * @return array Data of all connected hiorg users
     */
    public function loginGetOrgas($nc_uid)
    {
        return $this->hiorgLoginUsersDAO->loginGetOrgas($nc_uid);
    }
    /**
     * Get Data of login
     *
     * @param string $nc_uid
     * @return array Data of all connected hiorg users
     */
    public function loginGetData($hiorg_uid)
    {
        return $this->hiorgLoginUsersDAO->loginGetData($hiorg_uid);
    }


    /**
     *
     * @param string $hiorg_uid Hiorg User Id
     * @return string Nextcloud User Id
     */
    public function loginFindHiorgId($hiorg_uid)
    {
        return $this->hiorgLoginUsersDAO->loginFindHiorgId($hiorg_uid);
    }

    /**
     * @param string $nc_uid Nextcloud User Id
     * @param string $orga_name Orga Name
     * @return bool
     */
    public function loginIsAdmin($nc_uid, $orga_name) {
        return $this->hiorgLoginUsersDAO->loginIsAdmin($nc_uid, $orga_name);
    }

    /**
     * Check if group exists
     * Uses official NC API
     *
     * @param string $group_name
     * @return bool
     *
     */
    public function groupExists($group_name) {
        $groupObject = $this->groupManager->get($group_name);

        return ($groupObject instanceof \OCP\IGroup);
    }

    /**
     * Add group to system
     * Uses official NC API
     *
     * @param string $group_name
     */
    public function groupAdd($group_name) {
        if (!$this->groupExists($group_name)) {
            $this->groupManager->createGroup($group_name);
        }
    }

    /**
     * Remove group from system
     * Uses official NC API
     *
     * @param string $group_name
     */
    public function groupRemove($group_name) {
        $group = $this->groupManager->get($group_name);

        if ($group) {
            $group->delete();
        }
    }

    /**
     * Add User to group
     * Uses official NC API
     *
     * @param string $group_name
     * @param string $user_name
     */
    public function groupAddUser($group_name, $user_name) {
        $group = $this->groupManager->get($group_name);
        $user = $this->userManager->get($user_name);
        if (!$group || !$user) return null;

        $group->addUser($user);
    }

    /**
     * Remove User from group
     * Uses official NC API
     *
     * @param string $group_name
     * @param string $user_name
     */
    public function groupRemoveUser($group_name, $user_name) {
        $group = $this->groupManager->get($group_name);
        $user = $this->userManager->get($user_name);
        if (!$group || !$user) return null;

        $group->removeUser($user);
    }

    /**
     * Check if group is custom in orga
     * Uses official NC API
     *
     * @param string $group_name Name of Group
     * @param string $orga Name of Hiorg Orga
     * @return bool
     */
    public function groupIsInOrga($group_name, $orga) {

        /*
        if (
            ($group_name === $this->config->buildAdminGroupName($orga))
            || ($group_name === $this->config->buildUserGroupName($orga))
        ) {
            return false;
        }
        */

        $delimiter = $this->config->getAppValue('folder_delimiter');

        return (
            $group_name === $orga
            || (false !== strpos($group_name, $orga . $delimiter))
        );
    }

    /**
     * Get details of group
     * Uses official NC API
     *
     * @param string $group_name Name of group
     * @return array Details of group or empty if no match
     */
    public function groupGetDetails($group_name = null) {
        $groupObject = $this->groupManager->get($group_name);

        if (!$groupObject) return [];

        $groupUsers = $this->groupManager->displayNamesInGroup($groupObject->getGID(),'',1000,0);

        return [ 'name' => $group_name, 'users' => $groupUsers, 'fixed' => 0 ];
    }

    /**
     * Get all group folder details of Hiorg Orga
     * Uses official NC API
     *
     * @param $orga string Name of Hiorg orga
     * @return array Details of groups or empty if no match
     */
    public function groupListDetails($orga = null) {
        $delimiter = $this->config->getAppValue('group_delimiter');

        // get list of matching groups
        $search = $this->groupManager->search($orga);

        $result = [];
        if (0 < count($search)) {
            foreach ($search as $idx => $group) {
                $name = $group->getDisplayName();
                $groupUsers = $this->groupManager->displayNamesInGroup($group->getGID(),'',1000,0);

                if ($name === $this->config->buildAdminGroupName($orga)
                    || ($name === $this->config->buildUserGroupName($orga))
                ) {
                    $result[] = [ 'name' => $name, 'users' => $groupUsers, 'fixed' => 1 ];
                } elseif (($orga . $delimiter) === substr($name,0,strlen($orga . $delimiter))) {
                    $result[] = [ 'name' => $name, 'users' => $groupUsers, 'fixed' => 0 ];
                }
            }
        }
        return $result;
    }

    /**
     * Check if group has full access to groupfolder
     *
     * @param string $folder_name Name of Folder
     * @param string $group_name Name of Group
     * @return bool
     */
    public function groupfolderGroupHasFullAccess($folder_name, $group_name) {
        $folderObject = $this->groupfolderGetDetails($folder_name);

        return ($folderObject['groups'][$group_name] == 31);
    }

    /**
     * Check if group has full access to groupfolder
     *
     * @param string $folder_name Name of Folder
     * @param string $group_name Name of Group
     * @return bool
     */
    public function groupfolderGroupHasAccess($folder_name, $group_name) {
    /*
        $groupfolderId = $this->groupfolderGetId($folder_name);
        $folders = $this->folderManager->getFoldersForGroups([ $group_name ]);

        foreach ($folder as $folder) {
            if ($groupFolderId == (int)$folder['folder_id']) {
                return ($folder['permissions'] == 31); // Full Access
            }
        }
        return false;
*/
        $folderObject = $this->groupfolderGetDetails($folder_name);

        return isset($folderObject['groups'][$group_name]);
    }

    /**
     * Add Group folder to system
     * Uses inoffcial NC API
     *
     * @param string $folder_name Name of group folder
     * @param int $quota Quota in bytes
     */
    public function groupfolderAdd($folder_name, $admin_group_name, $quota) {
        $groupfolderId = $this->groupfolderGetId($folder_name);

        if ($groupfolderId == null) {
            $groupfolderId = $this->folderManager->createFolder($folder_name);
            $this->folderManager->setFolderQuota($groupfolderId, $quota);
        }
        if ($admin_group_name != null) {
            $this->folderManager->setFolderACL($groupfolderId, true);

            // dirty hack. First delete entry to avoid duplicate entry error
            $this->folderManager->setManageACL($groupfolderId, 'group', $admin_group_name, false);
            $this->folderManager->setManageACL($groupfolderId, 'group', $admin_group_name, true);

        }
        $this->groupfolderCache = null;
    }

    /**
     * Remove group folder from system
     * Uses inoffcial NC API
     *
     * @param string $folder_name Name of group folder
     */
    public function groupfolderRemove($folder_name) {
        $this->groupfolderCache = null;
        $groupfolderId = $this->groupfolderGetId($folder_name);

        if (!$groupfolderId) return;

        $this->folderManager->removeFolder($groupfolderId);
        $this->groupfolderCache = null;
    }

    /**
     * Add group to group folder
     * Uses inoffcial NC API
     *
     * @param string $folder_name Name of group folder
     * @param string $group_name Name of group to add
     * @param int $permission Permission to set
     */
    public function groupfolderAddGroup($folder_name, $group_name, $permission) {
        $groupfolderId = $this->groupfolderGetId($folder_name);
        $groupId = $this->groupManager->get($group_name);
        if (!$groupfolderId || !$groupId) return false;

        // User should always have full access
        if ($this->groupfolderGroupHasFullAccess($folder_name, $group_name) === false) {

            // User is new to folder
            if ($this->groupfolderGroupHasAccess($folder_name, $group_name) === false) {
                $this->folderManager->addApplicableGroup($groupfolderId, $group_name);
            }

            // Always set group permissions to full access
            $this->folderManager->setGroupPermissions($groupfolderId, $group_name, 31); // Always set to full access
        }


        // Set ACL
        if ($permission == 'ro') {
            $this->groupfolderSetGroupACL($folder_name,$group_name,31,1);
        } elseif ($permission == 'rw') {
            $this->groupfolderSetGroupACL($folder_name,$group_name,31,31);
        }

        $this->groupfolderCache = null;

    }

    /**
     * Remove group from group folder
     * Uses inoffcial NC API
     *
     * @param string $folder_name Name of group folder
     * @param string $group_name Name of group to add
     */
    public function groupfolderRemoveGroup($folder_name, $group_name) {
        $groupfolderId = $this->groupfolderGetId($folder_name);
        $groupId = $this->groupManager->get($group_name);
        if (!$groupfolderId || !$groupId) return false;

        $this->groupfolderSetGroupACL($folder_name,$group_name,0,0);
        $this->folderManager->removeApplicableGroup($groupfolderId, $group_name);
        $this->groupfolderCache = null;
    }

    /**
     * Check if groupfilder is in orga
     *
     * @param string $folder_name Name of Groupfolder
     * @param string $orga Name of Hiorg Orga
     * @return bool
     */
    public function groupfolderIsInOrga($folder_name, $orga) {
        $delimiter = $this->config->getAppValue('folder_delimiter');

        return (
            $folder_name === $orga
            || (0 === strpos($folder_name, $orga . $delimiter))
        );
    }

    /**
     * Check of group folder exists
     * Uses inoffcial NC API
     *
     * @param string $folder_name Name of group folder
     * @return bool
     *
     */
    public function groupfolderExists($folder_name) {
        return ($this->groupfolderGetId($folder_name) !== null);
    }

    /**
     * Get Id of group folder
     * Uses inoffcial NC API
     * Helper function
     *
     * @param string $folder_name Name of group folder
     * @return int Group Folder Id
     */
    private function groupfolderGetId($folder_name = null) {
        if (null === $folder_name) return null;

        if (!$this->groupfolderCache) {
            $this->groupfolderCache = $this->folderManager->getAllFolders();
        }

        if ($this->groupfolderCache) {
            foreach ($this->groupfolderCache as $id => $map) {
                if ($map['mount_point'] === $folder_name) {
                    return $id;
                }
            }
        }

        return null;
    }

    /**
     * Get single group folder details
     *
     * Helper function
     *
     * @param $folder_name string Name of group folder
     * @return array|null Details of group or null if nonexistant
     */
    public function groupfolderGetDetails($folder_name = null, $orga = null) {
        if (null === $folder_name) return null;

        if (!$this->groupfolderCache) {
            $this->groupfolderCache = $this->folderManager->getAllFolders();
        }

        $adminGroupName = $this->config->buildAdminGroupName($orga);

        $result = null;
        if ($this->groupfolderCache) {
            foreach ($this->groupfolderCache as $id => $map) {
                if ($map['mount_point'] === $folder_name) {

                    $permissions = [];
		    foreach ($map['groups'] as $group_name => $group_perm) {

                        $perm = $this->groupfolderGetGroupACL($id, $group_name);

                        // Sanitize permissions?
                        if ($group_name == $adminGroupName && $perm != 31) {
                            $this->groupfolderSetGroupACL($folder_name,$group_name,31,31);
                            $perm = $this->groupfolderGetGroupACL($id, $group_name);
                        }


                        $permissions[] = [
                            'name' => $group_name,
                            'perm' => $perm,
                            'fixed' => ($group_name == $adminGroupName
                                        || ($group_name == $orga && $folder_name == $orga)
                                        ),
                        ];

                    }

                    return [
                        'id' => $id,
                        'name' => $map['mount_point'],
                        'fixed' => $isFixed,
                        'groups' => $map['groups'],
                        'perm' => $permissions,
                        'quota' => $map['quota'],
                    ];

                }
            }
        }

        return null;
    }


    /**
     * Set permissions for group in groupfolder
     *
     * @param $folder_name string Name of group folder
     * @param $group_name string Name of group
     * @param $mask int Mask as integer
     * @param $permissions int Permissions as integer
     *
     */
    private function groupfolderSetGroupACL($folder_name, $group_name, $mask, $permissions) {
        $groupfolderId = $this->groupfolderGetId($folder_name);

        if ($groupfolderId == null) {
            return false;
        }

        // Get root path of groupfolder
        $jailPath = $this->mountProvider->getJailPath($groupfolderId);

        // Get folder details
        $folder = $this->folderManager->getFolder($groupfolderId, $this->rootFolder->getMountPoint()->getNumericStorageId());

        // Get mount point
	$mount = $this->mountProvider->getMount(
		$folder['id'],
		'/' . $folder['mount_point'],
		$folder['permissions'] ? $folder['permissions'] : 0,
		$folder['quota'],
		null,
		null,
		$folder['acl'],
		null
	);

        // Get id of root path
        $id = $mount->getStorage()->getCache()->getId('/');

        // Delete Rule
        if ($permissions == 0) {
            $this->ruleManager->deleteRule(new Rule(
                    new UserMapping('group', $group_name),
                    $id,
                    0,
                    0
            ));

        // Set rule
        } else {
            $this->ruleManager->saveRule(new Rule(
                    new UserMapping('group', $group_name),
                    $id,
                    $mask,
                    $permissions
            ));
        }
    }

    /**
     * Get permissions for group in groupfolder
     *
     * @param $folder_name string Name of group folder
     * @param $group_name string Name of group
     * @return int Permissions
     *
     */
    private function groupfolderGetGroupACL($folder_id, $group_name) {

        // Get root path of groupfolder
        $jailPath = $this->mountProvider->getJailPath($folder_id);
        // Get rules
        $rules = $this->ruleManager->getAllRulesForPaths($this->rootFolder->getMountPoint()->getNumericStorageId(), [ $jailPath ]);

        foreach ($rules[$jailPath] as $rid => $rule) {
            if ($rule->getUserMapping()->getType() == 'group'
                && $rule->getUserMapping()->getId() == $group_name) {
                    return $rule->getPermissions();
                }
        }
        return null;
    }

    /**
     * Get all group folder details of Hiorg Orga
     *
     * @param $orga string Name of Hiorg orga
     * @return array Details of groupfolders or empty if no match
     */
    public function groupfolderListDetails($orga = null) {
        if (null === $orga) return null;

        $delimiter = $this->config->getAppValue('folder_delimiter');
        $userFolderName = $this->config->buildUserFolderName($orga);
        $adminFolderName = $this->config->buildAdminFolderName($orga);
        $adminGroupName = $this->config->buildAdminGroupName($orga);

        if (!$this->groupfolderCache) {
            $this->groupfolderCache = $this->folderManager->getAllFolders();
        }

        $result = null;
        if ($this->groupfolderCache) {
            foreach ($this->groupfolderCache as $id => $map) {

                $folder_name = $map['mount_point'];

                $isInOrga = false;
                if ($folder_name === $adminFolderName
                    || $folder_name === $userFolderName
                ) {

                    $isInOrga = true;
                    $isFixed = true;
                } elseif (false !== strpos ($folder_name, $orga . $delimiter)) {

                    $isInOrga = true;
                    $isFixed = false;
                }

                if ($isInOrga == true) {

                    $permissions = [];
		    foreach ($map['groups'] as $group_name => $group_perm) {

                        $perm = $this->groupfolderGetGroupACL($id, $group_name);

                        // Sanitize ACL state
                        if ($map['acl'] == false) {
                            $this->folderManager->setFolderACL($id, true);
                            $this->folderManager->setManageACL($id, 'group', $adminGroupName, false);
                            $this->folderManager->setManageACL($id, 'group', $adminGroupName, true);
                        }

                        // Sanitize permissions?
                        if ($group_name == $adminGroupName && $perm != 31) {

                            $this->groupfolderSetGroupACL($folder_name, $group_name,31,31);
                            $perm = $this->groupfolderGetGroupACL($id, $group_name);
                        }

                        $permissions[] = [
                            'name' => $group_name,
                            'perm' => $perm,
                            'fixed' => ($group_name == $adminGroupName
                                        || ($group_name == $orga && $folder_name == $orga)
                                        ),
                        ];

                    }

                    $result[] = [
                        'id' => $id,
                        'name' => $folder_name,
                        'fixed' => $isFixed,
                        'groups' => $map['groups'],
                        'perm' => $permissions,
                        'quota' => $map['quota'],
                    ];
                }
            }
        }

        return $result;
    }

}
