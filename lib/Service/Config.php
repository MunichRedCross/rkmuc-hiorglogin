<?php
/**
 *
 * @copyright Copyright (c) 2019, Björn A. Bores <bjalbor@gmail.com>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\HiorgLogin\Service;

use OCP\IConfig;
use OCP\IURLGenerator;

class Config {
    /** @var IConfig */
    private $config;
    /** @var IURLGenerator */
    private $urlGenerator;
    /** @var array */
    private $default;

    public function __construct(
        $appName,
        IURLGenerator $urlGenerator,
        IConfig $config
    ) {
        $this->appName = $appName;
        $this->config = $config;

        $this->urlGenerator = $urlGenerator;

        // Set defaults
        $this->default = array(
            // 'munich mode': Get all connections in one from Hiorg
            'autoconnect' => false,
            // Allow to connect other hiorg users
            'allow_login_connect' => true,
            // Delimiter for building group names
            'group_delimiter' => '-',
            // Suffix to append to admin group
            'group_suffix_admin' => 'leitung',
            // Suffix to append to normal user group
            'group_suffix_user' => null,
            // Delimiter for building folder names
            'folder_delimiter' => '-',
            // Suffix to append to admin folder
            'folder_suffix_admin' => 'leitung',
            // Suffix to append to normal user folder
            'folder_suffix_user' => null,
            // Prefix for building Nextcloud user name
            'nc_uid_prefix' => 'hiorg-',
            // Allowed Orgas
            'allowed_orgas' => null,
            // Default folder quota
            'default_folder_quota' => 0,
            // OAuth2 URIs
            'oauth2_clientId' => '',
            'oauth2_clientSecret' => '',
            'oauth2_scope' => 'openid',
            'oauth2_urlAuthorize' => 'https://api.hiorg-server.de/oauth/v1/authorize',
            'oauth2_urlAccessToken' => 'https://api.hiorg-server.de/oauth/v1/token',
            'oauth2_urlResourceOwnerDetails' => 'https://api.hiorg-server.de/oauth/v1/userinfo'
        );
    }

    /**
     * @param string $orga
     * @param string $delimiter
     * @param string $suffix
     * @return string complete name
     */
    private function buildName($orga, $delimiter, $suffix) {
        return (strlen($delimiter) > 0 && strlen($suffix) > 0) ? $orga . $delimiter . $suffix : $orga;
    }

    /**
     * @param string $key config key to return
     * @return string config value 
     */
    public function getAppValue($key) {
        $result = $this->config->getAppValue($this->appName, $key);
        return $result ? $result : $this->default[$key];
    }

    /**
     * @param string $key config key to set
     * @param string $value config value to set
     */
    public function setAppValue($key, $value) {
        $result = $this->config->setAppValue($this->appName, $key, $value);
    }

    /**
     * @param string $uid user id
     * @param string $key config key
     * @param string $default default value
     * @return string config value 
     */
    public function getUserValue($uid, $key, $default) {
        return $this->config->getUserValue($uid, $this->appName, $key, $default);
    }

    /**
     * @param string $uid user id
     * @param string $key config key
     * @param string $value config value to set
     */
    public function setUserValue($uid, $key, $value) {
        return $this->config->setUserValue($uid, $this->appName, $key, $value);
    }

    /**
     * @param string $hiorg_uid hiorg user id
     * @return string nextcloud user id
     */
    public function buildNcUid($hiorg_uid) {
        return $this->getAppValue('nc_uid_prefix') . $hiorg_uid;
    }

    /** 
     * @param string $orga orga
     * @return string complete name
     */
    public function buildAdminGroupName($orga) {
        $delimiter = $this->getAppValue('group_delimiter');
        $suffix = $this->getAppValue('group_suffix_admin');

        return $this->buildName($orga, $delimiter, $suffix);
    }

    /** 
     * @param string $orga orga
     * @return string complete name
     */
    public function buildUserGroupName($orga) {
        $delimiter = $this->getAppValue('group_delimiter');
        $suffix = $this->getAppValue('group_suffix_user');

        return $this->buildName($orga, $delimiter, $suffix);
    }

    public function buildGroupName($orga, $suffix) {
        $delimiter = $this->getAppValue('group_delimiter');

        return $this->buildName($orga, $delimiter, $suffix);
    }

    /** 
     * @param string $orga orga
     * @return string complete name
     */
    public function buildAdminFolderName($orga) {
        $suffix = $this->getAppValue('folder_suffix_admin');

        return $this->buildFolderName($orga, $suffix);
    }

    /** 
     * @param string $orga orga
     * @return string complete name
     */
    public function buildUserFolderName($orga) {
        $suffix = $this->getAppValue('folder_suffix_user');

        return $this->buildFolderName($orga, $suffix);
    }

    /** 
     * @param string $orga orga
     * @param string $suffix
     * @return string complete name
     */
    public function buildFolderName($orga, $suffix) {
        $delimiter = $this->getAppValue('folder_delimiter');

        return $this->buildName($orga, $delimiter, $suffix);
    }

}
