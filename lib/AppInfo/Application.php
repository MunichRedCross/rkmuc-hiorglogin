<?php
/**
 *
 * @copyright Copyright (c) 2019, Björn A. Bores <bjalbor@gmail.com>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\HiorgLogin\AppInfo;

use OCP\AppFramework\App;
use OCP\IURLGenerator;

class Application extends App
{
    /** @var string */
    private $appName = 'hiorglogin';
    /** @var string */
    private $providerUrl;
    /** @var IURLGenerator */
    private $urlGenerator;

    public function __construct(
    ) {
        parent::__construct($this->appName);

        $userBackendHiorgLogin = \OC::$server
                ->query(\OCA\HiorgLogin\UserBackend::class);

        \OC::$server->getUserManager()->registerBackend($userBackendHiorgLogin);

        $this->urlGenerator = $this->query(IURLGenerator::class);

    }

    public function register()
    {
                $redirect_url = null;
                if (isset ($_GET['redirect_url'])) {
                    $redirect_url = $_GET['redirect_url'];
                }
                $this->providerUrl = $this->urlGenerator->linkToRoute($this->appName.'.login.authlogin', [ 'redirect_url' => $redirect_url ]);

                \OC_App::registerLogIn([
                    'name' => 'HiOrg Login',
                    'href' => $this->providerUrl,
                ]);
    }

    private function query($className)
    {
        return $this->getContainer()->query($className);
    }
    
}
