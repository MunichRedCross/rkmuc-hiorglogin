<?php
/**
 *
 * @copyright Copyright (c) 2019, Björn A. Bores <bjalbor@gmail.com>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\HiorgLogin\Settings;

use OCP\AppFramework\Http\TemplateResponse;
use OCP\Settings\ISettings;
use OCP\IGroupManager;
use OCP\IURLGenerator;
use OCP\IConfig;
use OCP\Util;

class AdminSettings implements ISettings
{
    /** @var string */
    private $appName;
    /** @var IConfig */
    private $config;
    /** @var IURLGenerator */
    private $urlGenerator;
    /** @var IGroupManager */
    private $groupManager;

    public function __construct($appName, IConfig $config, IURLGenerator $urlGenerator, IGroupManager $groupManager)
    {
        $this->appName = $appName;
        $this->config = $config;
        $this->urlGenerator = $urlGenerator;
        $this->groupManager = $groupManager;
    }

    public function getForm()
    {
        Util::addStyle($this->appName, 'settings');
        Util::addScript($this->appName, 'settings');
        $paramsNames = [
            'default_user_group',
            'default_admin_group',
            'oauth2_clientId',
            'oauth2_clientSecret',
            'allowed_orgas',
            'default_folder_quota',
            'allow_login_connect',
        ];

        // All group names
        $groupNames = [];
        $groups = $this->groupManager->search('');
        foreach ($groups as $group) {
            $groupNames[] = $group->getGid();
        }

        // Quota selection
        $quotas = [];
        $quotas[] = array( 'value' => 128*1024*1024, 'text' => '128 MB');
        $quotas[] = array( 'value' => 256*1024*1024, 'text' => '256 MB');
        $quotas[] = array( 'value' => 512*1024*1024, 'text' => '512 MB');
        $quotas[] = array( 'value' => 1024*1024*1024, 'text' => '1 GB');
        $quotas[] = array( 'value' => 2*1024*1024*1024, 'text' => '2 GB');
        $quotas[] = array( 'value' => 5*1024*1024*1024, 'text' => '5 GB');
        $quotas[] = array( 'value' => 10*1024*1024*1024, 'text' => '10 GB');

        $params = [
            'action_url' => $this->urlGenerator->linkToRoute($this->appName.'.settings.saveadmin'),
            'requesttoken' => Util::callRegister(),
            'groups' => $groupNames,
            'quotas' => $quotas,
        ];

        foreach ($paramsNames as $paramName) {
            $params[$paramName] = $this->config->getAppValue($this->appName, $paramName);
        }

        return new TemplateResponse($this->appName, 'admin', $params);
    }

    public function getSection()
    {
        return $this->appName;
    }

    public function getPriority()
    {
        return 0;
    }
}
