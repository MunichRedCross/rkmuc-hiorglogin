<?php
/**
 *
 * @copyright Copyright (c) 2019, Björn A. Bores <bjalbor@gmail.com>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\HiorgLogin\Settings;

use OCP\AppFramework\Http\TemplateResponse;
use OCP\Settings\ISettings;
use OCP\IURLGenerator;
use OCP\IUserSession;
use OCP\Util;
use OCA\HiorgLogin\Service\Service;
use OCA\HiorgLogin\Service\Config;

class PersonalSettings implements ISettings
{
    /** @var string */
    private $appName;
    /** @var IURLGenerator */
    private $urlGenerator;
    /** @var IUserSession */
    private $userSession;
    /** @var Service */
    private $service;
    /** @var Config */
    private $config;

    public function __construct(
        $appName,
        IURLGenerator $urlGenerator,
        IUserSession $userSession,
        Service $service,
        Config $config
    ) {
        $this->appName = $appName;
        $this->urlGenerator = $urlGenerator;
        $this->userSession = $userSession;
        $this->service = $service;
        $this->config = $config;
    }

    public function getForm()
    {
        Util::addScript($this->appName, 'settings');
        $uid = $this->userSession->getUser()->getUID();
        $params = [
            'api_group' => $this->urlGenerator->linkToRoute($this->appName.'.settings.groupindex'),
            'api_folder' => $this->urlGenerator->linkToRoute($this->appName.'.settings.folderindex'),
            'requesttoken' => Util::callRegister(),
            'connect_url' => $this->urlGenerator->linkToRoute($this->appName.'.login.authlogin'),
            'providers' => [],
            'main_login' => '',
            'connected_logins' => [],
            'allow_login_connect' => $this->config->getAppValue('allow_login_connect'),
        ];

        if (true || $params['allow_login_connect']) {
            $connectedLogins = $this->service->loginGetConnected($uid);
            foreach ($connectedLogins as $login) {
                $hiorg_data = $this->service->loginGetData($login);
                $description = $hiorg_data['fullname'] . ' (' . $hiorg_data['orga'] . ')';

                if (strcmp($this->config->buildNcUid($hiorg_data['user_id']), $uid) == 0) {
                    $params['main_login'] = $description;
                } else {
                    $params['connected_logins'][$description] = $this->urlGenerator->linkToRoute($this->appName.'.settings.logindisconnect', [
                        'login' => $login,
                        'requesttoken' => Util::callRegister(),
                    ]);
                }
            }
        }

        $orgas = $this->service->loginGetOrgas($uid);
        if (!isset($orgas)) $orgas = [];

        $manageableOrgas = [];
        foreach ($orgas as $o) {
            if (true === $this->service->loginIsAdmin($uid, $o)) {
                $listFix = [];
                $listFree = [];

                // get list of groups
                $a = $this->service->groupListDetails($o);
                
                $manageableOrgas[] = $o;
                $b[] = $a;
                $groupListFree[ $o ] = $listFree;
                $groupListFix[ $o ] = $listFix;
            }
        }
        $params['manageable_orgas'] = $manageableOrgas;
        $params['group_list_fix'] = $groupListFix;
        $params['group_list_free'] = $groupListFree;
        $params['server_data'] = $groupListFree; //var_export($data[0],true);

        return new TemplateResponse($this->appName, 'personal', $params);
    }

    public function getSection()
    {
        if ('Hiorg Login' === $this->userSession->getUser()->getBackendClassName()) {
            return 'hiorglogin';
        } else {
            return null;
        }
    }

    public function getPriority()
    {
        return 0;
    }
}
