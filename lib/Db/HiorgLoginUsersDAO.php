<?php
/**
 *
 * @copyright Copyright (c) 2019, Björn A. Bores <bjalbor@gmail.com>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\HiorgLogin\Db;

use OCP\DB\QueryBuilder\IQueryBuilder;
use OCP\IDBConnection;

class HiorgLoginUsersDAO
{
    /** @var Db */
    private $db;

    public function __construct(IDBConnection $db)
    {
        $this->db = $db;
    }

    /**
     * Get all users matching displayname
     * 
     * @param string $searchstring Display Name to search
     * @param integer $limit Limit for paging
     * @param integer $offser Offset for Paging
     * @return array
     */
    public function loginFindDisplayName($searchstring = "", $limit = null, $offset = null)
    {
        $qb = $this->db->getQueryBuilder();

        $qb->select('nc_uid')
            ->from('hiorglogin_users')
            ->where($qb->expr()->like(
                    'displayname',
                    $qb->createNamedParameter('%'.$searchstring.'%'),
                    IQueryBuilder::PARAM_STR
                    )
            )
            ->setMaxResults($limit)
            ->setFirstResult($offset);

        $cursor = $qb->execute();
        $res = [];
        while ($row = $cursor->fetch()) {
            $res[] = $row['nc_uid'];
        }
        $cursor->closeCursor();

        return $res;
    }

    /**
     * @param string $hiorg_uid hiorg login identifier
     * @return string|null User nextcloud login identifier
     */
    public function loginFindHiorgId($hiorg_uid)
    {
        $qb = $this->db->getQueryBuilder();

        $qb->select('nc_uid')
            ->from('hiorglogin_users')
            ->where($qb->expr()->eq(
                    'hiorg_uid',
                    $qb->createNamedParameter($hiorg_uid, IQueryBuilder::PARAM_STR)
                    )
            );

        $cursor = $qb->execute();
        $row = $cursor->fetch();
        $cursor->closeCursor();

        return $row ? $row['nc_uid'] : null;
    }

    /**
     * @param string $nc_uid nextcloud login identifier
     * @return bool
     */
    public function loginExists($nc_uid)
    {
        $qb = $this->db->getQueryBuilder();

        $qb->select('hiorg_data')
            ->from('hiorglogin_users')
            ->where($qb->expr()->eq(
                    'nc_uid',
                    $qb->createNamedParameter($nc_uid, IQueryBuilder::PARAM_STR)
                    )
            );

        $cursor = $qb->execute();
        $row = $cursor->fetch();
        $cursor->closeCursor();

        return $row ? true : false;
    }

    /**
     * Add login connection
     * 
     * @param string $nc_uid nextcloud login identifier
     * @param string $hiorg_uid hiorg login identifier
     * @param string $displayname display name
     * @param string $data hiorg user data
     */
    public function loginAdd($nc_uid, $hiorg_uid, $displayname, $data)
    {
        $qb = $this->db->getQueryBuilder();

        $qb->insert('hiorglogin_users')
            ->values([
                    'nc_uid' => $qb->createNamedParameter($nc_uid, IQueryBuilder::PARAM_STR),
                    'hiorg_uid' => $qb->createNamedParameter($hiorg_uid, IQueryBuilder::PARAM_STR),
                    'displayname' => $qb->createNamedParameter($displayname, IQueryBuilder::PARAM_STR),
                    'hiorg_data' => $qb->createNamedParameter(serialize($data), IQueryBuilder::PARAM_STR),
                    ]
            );

        return ($qb->execute()) ? true : false;
    }

    /**
     * Update login connection
     * 
     * @param string $nc_uid nextcloud login identifier
     * @param string $hiorg_uid hiorg login identifier
     * @param string $displayname display name
     * @param string $data hiorg user data
     */
    public function loginUpdate($nc_uid, $hiorg_uid, $displayname, $data)
    {
        $qb = $this->db->getQueryBuilder();

        $qb->update('hiorglogin_users')
            ->set('displayname', $qb->createNamedParameter($displayname, IQueryBuilder::PARAM_STR))
            ->set('hiorg_data', $qb->createNamedParameter(serialize($data), IQueryBuilder::PARAM_STR))
            ->where($qb->expr()->eq(
                    'nc_uid',
                    $qb->createNamedParameter($nc_uid, IQueryBuilder::PARAM_STR)
                    )
            )
            ->andWhere($qb->expr()->eq(
                    'hiorg_uid',
                    $qb->createNamedParameter($hiorg_uid, IQueryBuilder::PARAM_STR)
                    )
            );

        return ($qb->execute()) ? true : false;
    }

    /**
     * Disconnect hiorg login from nextcloud user
     * 
     * @param string $hiorg_uid hiorg login identifier
     * @return bool
     */
    public function loginDisconnect($hiorg_uid)
    {
        $qb = $this->db->getQueryBuilder();

        $qb->delete('hiorglogin_users')
            ->where($qb->expr()->eq(
                    'hiorg_uid',
                    $qb->createNamedParameter($hiorg_uid, IQueryBuilder::PARAM_STR)
                    )
            );

        return ($qb->execute()) ? true : false;
    }

    /**
     * Check if user is admin of orga
     * 
     * @param string $nc_uid Nextcloud Userid
     * @param string $orga_name Orga Name
     * @return bool
     */
    public function loginIsAdmin($nc_uid = null, $orga_name = null)
    {
        $qb = $this->db->getQueryBuilder();

        $qb->select('hiorg_data')
            ->from('hiorglogin_users')
            ->where($qb->expr()->eq(
                    'nc_uid',
                    $qb->createNamedParameter($nc_uid, IQueryBuilder::PARAM_STR)
                    )
            );

        $cursor = $qb->execute();
        while ($row = $cursor->fetch()) {
            $data = unserialize($row['hiorg_data']);
            if ($data['orga'] == $orga_name) {
                return ($data['leitung'] == true);
            }
        }
        $cursor->closeCursor();

        return false;
    }

    /**
     * Get all connected logins
     * 
     * @param string $nc_uid Nextcloud Userid
     * @return array Connected Hiorg UserIds
     */
    public function loginGetConnected($nc_uid)
    {
        $qb = $this->db->getQueryBuilder();

        $qb->select('hiorg_uid')
            ->from('hiorglogin_users')
            ->where($qb->expr()->eq(
                    'nc_uid',
                    $qb->createNamedParameter($nc_uid, IQueryBuilder::PARAM_STR)
                    )
            );

        $cursor = $qb->execute();
        $result = [];
        while ($row = $cursor->fetch()) {
            $result[] = $row['hiorg_uid'];
        }
        $cursor->closeCursor();

        return $result;
    }

    /**
     * Get Orgas of user
     * 
     * @param string $nc_uid
     * @return array List of all orgas
     */
    public function loginGetOrgas($nc_uid)
    {
        $qb = $this->db->getQueryBuilder();

        $qb->select('hiorg_data')
            ->from('hiorglogin_users')
            ->where($qb->expr()->eq(
                    'nc_uid',
                    $qb->createNamedParameter($nc_uid, IQueryBuilder::PARAM_STR)
                    )
            );

        $cursor = $qb->execute();
        $result = [];
        while ($row = $cursor->fetch()) {
            $result[] = unserialize($row['hiorg_data'])['orga'];
        }
        $cursor->closeCursor();

        return $result;
    }

    /**
     * Get Data of login
     * 
     * @param string $hiorg_uid nextcloud login identifier
     * @return string|null Diplayname
     */
    public function loginGetData($hiorg_uid)
    {
        $qb = $this->db->getQueryBuilder();

        $qb->select('hiorg_data')
            ->from('hiorglogin_users')
            ->where($qb->expr()->eq(
                    'hiorg_uid',
                    $qb->createNamedParameter($hiorg_uid, IQueryBuilder::PARAM_STR)
                    )
            );

        $cursor = $qb->execute();
        $row = $cursor->fetch();

        return $row ? unserialize($row['hiorg_data']) : null;
    }

    /**
     * Get Display Name of user
     * 
     * @param string $nc_uid nextcloud login identifier
     * @return string|null Diplayname
     */
    public function getDisplayName($nc_uid)
    {
        $qb = $this->db->getQueryBuilder();

        $qb->select('displayname')
            ->from('hiorglogin_users')
            ->where($qb->expr()->eq(
                    'nc_uid',
                    $qb->createNamedParameter($nc_uid, IQueryBuilder::PARAM_STR)
                    )
            );

        $cursor = $qb->execute();
        $row = $cursor->fetch();

        return $row ? $row['displayname'] : null;
    }

    /**
     * Get E-Mail-Addres of user
     * 
     * @param string $nc_uid nextcloud login identifier
     * @return string|null E-Mail-Adress
     */
    public function getEMailAddress($nc_uid)
    {
        $qb = $this->db->getQueryBuilder();

        $qb->select('hiorg_data')
            ->from('hiorglogin_users')
            ->where($qb->expr()->eq(
                    'nc_uid',
                    $qb->createNamedParameter($nc_uid, IQueryBuilder::PARAM_STR)
                    )
            );

        $cursor = $qb->execute();
        $row = $cursor->fetch();

        $data = unserialise($row['hiorg_data']);

        return $row ? $data['email'] : null;
    }

    /**
     * Delete nextcloud user
     * 
     * @param string $nc_uid nextclous login identifier
     * @return bool
     */
    public function deleteUser($nc_uid)
    {
        $qb = $this->db->getQueryBuilder();

        $qb->delete('hiorglogin_users')
            ->where($qb->expr()->eq(
                    'nc_uid',
                    $qb->createNamedParameter($nc_uid, IQueryBuilder::PARAM_STR)
                    )
            );

        return ($qb->execute()) ? true : false;
    }

}
