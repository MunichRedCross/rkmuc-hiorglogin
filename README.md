HiOrg Login
===========
**Authenticate user login against [HiOrg-Server](https://www.hiorg-server.de/)**.

Passwords are not stored locally; authentication always happens against
the remote server. Users can connect more than one HiOrg logins to one single Nextcloud user.

Special users (Leitungskräfte) are allowed to manage groups and groupfolders within their scope.

For every hiorg orga two groupfolders are created at first login:
- ***name of hiorg orga*** (r/o for normal users, r/w for Leitungskräfte)
- ***name of hiorg orga***-**leitung** (only visible to Leitungskräfte, r/w)

Every user gets membership to following group(s) on login:
- ***name of hiorg orga***
- ***name of hiorg orga***-**leitung** (only Leitungskräfte)

See section Configuration for changing delimiter and suffixes.

User data and their display name is stored in an own database table ``hiorglogin_users``.

## Dependencies

- Nexcloud > 15 (actually tested with NC15 and NC18)
- Nextcloud app ``groupfolders``
- Nextcloud app ``groupquota``

You will need an OAuth2-Key (see https://wiki.hiorg-server.de/admin/oauth2). Use ``https://server.domain.tld/apps/hiorglogin/auth`` as redirect url.

## Installation

Place this app in **nextcloud/apps/** 

```
cd ${NEXTCLOUDOOT}/apps
git clone https://gitlab.com/MunichRedCross/rkmuc-hiorglogin.git hiorglogin
cd hiorglogin
make
```

and enable in admin panel.

## Configuration

All neccessary configuration is done as admin user in settings / Administration / HiOrg Login:

- *Default group for all users from HiOrg*: All users logged in from HiOrg will be member of this group
- *Default group for all admin users from HiOrg*: All admin users logged in from Hiorg will be member of this group
- *Default quota for group folders*: New groupfolders will get this quota (default Unlimited)
- *Client Id*: OAuth2 user
- *Client Secret*: OAuth2 secret
- *Allowed Orgs*: Restrict Logins to this HiOrg Organisationseinheiten 

You can define some special configuration in ``Config.php``

- *group_delimiter*: Use this delimiter for group names within an orga (default ``-``)
- *group_suffix_user*: Suffix for default usergroup (default none)
- *group_suffix_admin*: Suffix for admin usergroup (default ``leitung``)
- *folder_delimiter*: Use this delimiter for groupfolder names within an orga (default ``-``)
- *folder_suffix_user*: Suffix for default groupfolder (default none)
- *folder_suffix_admin*: Suffix for default admin groupfolder (default ``leitung``)
- *nc_uid_prefix*: prefix for hiorg userids (default ``hiorg-``)

## Caveats

On every login users data (email address and display name) is updated to make sure not to have outdated data. If a user has more than one HiOrg logins connected with different email addresses this can lead to the effect, that the nextcloud email address will alternate according to the used login (same with display name). Nextcloud will generate an email to the user every time this happens.

Membership to Leitung is updated on login. If a user loses membership in Hiorg but does not login to Nexcloud using this connection (s)he will keep the membership in Nextcloud.

## Questions?

Feel free to drop me an email at bjalbor -at- gmail.com if you need help.

## Issues

If you encounter an error or have a feature request, please create an issue right here on https://gitlab.com/MunichRedCross/rkmuc-hiorglogin/-/issues.

