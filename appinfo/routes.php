<?php
/**
 * Create your routes in here. The name is the lowercase name of the controller
 * without the controller part, the stuff after the hash is the method.
 * e.g. page#index -> OCA\HiorgLogin\Controller\PageController->index()
 *
 * The controller class has to be registered in the application.php file since
 * it's instantiated in there
 */
return [
    'routes' => [
        ['name' => 'login#authlogin', 'url' => '/auth', 'verb' => 'GET'],
        ['name' => 'login#authlogin', 'url' => '/auth', 'postfix' => '.post', 'verb' => 'POST'],
        ['name' => 'settings#logindisconnect', 'url' => '/disconnect/{login}', 'verb' => 'GET'],
        ['name' => 'settings#saveadmin', 'url' => '/settings/admin', 'verb' => 'POST'],
        ['name' => 'settings#savepersonal', 'url' => '/settings/personal', 'verb' => 'POST'],
        ['name' => 'settings#groupindex', 'url' => '/settings/groups/', 'verb' => 'GET'],
        ['name' => 'settings#groupshow', 'url' => '/settings/groups/{orga}', 'verb' => 'GET'],
        ['name' => 'settings#groupadd', 'url' => '/settings/groups/{orga}', 'verb' => 'POST'],
        ['name' => 'settings#groupdelete', 'url' => '/settings/groups/{orga}/{group}', 'verb' => 'DELETE'],
        ['name' => 'settings#groupuseradd', 'url' => '/settings/groups/{orga}/{group}', 'verb' => 'POST'],
        ['name' => 'settings#groupuserdelete', 'url' => '/settings/groups/{orga}/{group}/{user}', 'verb' => 'DELETE'],
        ['name' => 'settings#usersearch', 'url' => '/settings/user/', 'verb' => 'GET'],
        ['name' => 'settings#folderindex', 'url' => '/settings/folders/', 'verb' => 'GET'],
        ['name' => 'settings#foldershow', 'url' => '/settings/folders/{orga}', 'verb' => 'GET'],
        ['name' => 'settings#folderadd', 'url' => '/settings/folders/{orga}', 'verb' => 'POST'],
        ['name' => 'settings#folderdelete', 'url' => '/settings/folders/{orga}/{folder}', 'verb' => 'DELETE'],
        ['name' => 'settings#foldergroupadd', 'url' => '/settings/folders/{orga}/{folder}', 'verb' => 'POST'],
        ['name' => 'settings#foldergroupdelete', 'url' => '/settings/folders/{orga}/{folder}/{group}', 'verb' => 'DELETE'],
    ]
];
