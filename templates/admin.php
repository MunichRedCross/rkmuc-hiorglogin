<?php
/** @var array $_ */
/** @var \OCP\IL10N $l */

?>

<div id="hiorglogin" class="section">
    <form id="hiorglogin_settings" action="<?php print_unescaped($_['action_url']) ?>" method="post">
        <input type="hidden" value="<?php p($_['requesttoken']); ?>" name="requesttoken" />
        <p>
        <label for="default_user_group"><?php p($l->t('Default group for all users from HiOrg')); ?></label>
        <select id="default_user_group" name="default_user_group">
            <option value=""><?php p($l->t('None')); ?></option>
            <?php foreach ($_['groups'] as $group): ?>
                <option value="<?php p($group) ?>" <?php p($_['default_user_group'] === $group ? 'selected' : '') ?>><?php p($group) ?></option>
            <?php endforeach ?>
        </select>
        <br>
        <label for="default_admin_group"><?php p($l->t('Default group for all admins users from HiOrg')); ?></label>
        <select id="default_admin_group" name="default_admin_group">
            <option value=""><?php p($l->t('None')); ?></option>
            <?php foreach ($_['groups'] as $group): ?>
                <option value="<?php p($group) ?>" <?php p($_['default_admin_group'] === $group ? 'selected' : '') ?>><?php p($group) ?></option>
            <?php endforeach ?>
        </select>
        <br>
        <label for="default_folder_quota"><?php p($l->t('Default quota for group folders')); ?></label>
        <select id="default_folder_quota" name="default_folder_quota">
            <option value=""><?php p($l->t('Unlimited')); ?></option>
            <?php foreach ($_['quotas'] as $idx => $quota): ?>
                <option value="<?php p($quota['value']) ?>" <?php p($_['default_folder_quota'] == $quota['value'] ? 'selected' : '') ?>><?php p($quota['text']) ?></option>
            <?php endforeach ?>
        </select>
        <div>
            <input id="allow_login_connect" type="checkbox" class="checkbox" name="allow_login_connect" value="1" <?php p($_['allow_login_connect'] ? 'checked' : '') ?>/>
            <label for="allow_login_connect"><?php p($l->t('allow_login_connect')) ?></label>
        </div>
        <div>
            <label>
                <?php p($l->t('Client Id')) ?><br>
                <input type="text" name="oauth2_clientId" value="<?php p($_['oauth2_clientId']) ?>">
            </label>
        </div>
        <div>
            <label>
                <?php p($l->t('Client Secret')) ?><br>
                <input type="text" name="oauth2_clientSecret" value="<?php p($_['oauth2_clientSecret']) ?>">
            </label>
        </div>
        <hr/>
        <div>
            <label for="allowed_orgas"><?php p($l->t('Allowed Orgs')) ?></label>
            <textarea name="allowed_orgas" style="width: 100%;"><?php p($_['allowed_orgas']) ?></textarea>
        </div>
        </p>
        <button><?php p($l->t('Save')); ?></button>
</div>
