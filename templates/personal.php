<?php
script('hiorglogin', 'personal');
?>
<span id="params" data-params="<?php p(json_encode($_));?>"></span>
<div>
    <h2>Mit diesem Benutzer verknüpfte HiOrg-Zugänge</h2>
        <ul>
        <li>
            <span class="icon-home">&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<?php p($_['main_login']) ?>
        </li>
            <?php foreach ($_['connected_logins'] as $title=>$url): ?>
	    <li><a href="<?php print_unescaped($url) ?>">
		<span class="icon-delete" title="Verknüpfung zu <?php p($title) ?> entfernen">&nbsp;&nbsp;&nbsp;&nbsp;</span>
		</a>
			<?php p($title) ?>
	    </li>
            <?php endforeach ?>
        </ul>
    <?php if ($_['allow_login_connect']): ?>
        <?php if (0 === count($_['connected_logins'])): ?>
            Bisher sind keine weiteren HiOrg-Zugänge verknüpft
            <br>
        <?php endif ?>
        <br>
        <a class="button" href="<?php print_unescaped($_['connect_url']); ?>">Weiteren HiOrg-Zugang verknüpfen</a>
        <br>Um einen neuen Zugang verknüpfen zu können, muss zunächst die gültige Anmeldung beim HiOrg-Server im Browser gelöscht werden. Dies kann über den Link <a target="_blank" href="https://www.hiorg-server.de/logout.php">https://www.hiorg-server.de/logout.php</a> erfolgen.
    <?php else: ?>
        <br>
        <a class="button" href="#">Verknüpfungen sind nicht erlaubt</a>
    <?php endif ?>
        <br><br>
</div>
<hr>
<div id="orga_settings" data-params="<?php p(json_encode($_));?>"></div>
