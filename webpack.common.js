const path = require('path')
const { VueLoaderPlugin } = require('vue-loader');

module.exports = {
  entry: {
          'personal': './src/personal.js',
  },
  output: {
    path: path.resolve(__dirname, './js'),
    publicPath: '/',
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'vue-style-loader', 'css-loader'
        ],
      },
      {
        test: /\.scss$/,
        use: [
          'vue-style-loader', 'css-loader', 'sass-loader'
        ],
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
      },
      {
        test:  /\.(ttf|eot|svg)$/,
        loader: 'file-loader',
      },
      {
        test:  /\.woff(2)?$/,
        loader: 'url-loader?limit=10000&mimetype=application/font-woff',
      },
      {
        test: /\.(gif|jpe?g|png)$/,
        loader: 'url-loader?limit=25000',
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
    ]
  },
  plugins: [
    new VueLoaderPlugin()
  ],
  resolve: {
    extensions: ['*', '.js', '.vue', '.json']
  }
}
